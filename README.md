kubectl create secret docker-registry gitlab-token-auth \
  --docker-server=https://registry.gitlab.com \
  --docker-username=token \
  --docker-password="xxx" \
  --docker-email=moscich@gmail.com

helm repo add gitlab https://charts.gitlab.io            
helm repo update
helm upgrade --install test gitlab/gitlab-agent \
    --namespace gitlab-agent-test \
    --create-namespace \
    --set image.tag=v16.5.0 \
    --set config.token=xxx \
    --set config.kasAddress=wss://kas.gitlab.com